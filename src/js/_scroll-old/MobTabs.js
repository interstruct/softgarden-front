import Swiper from 'swiper/dist/js/swiper';

export default class MobTabs {
  constructor(tabs) {
    this.tabs = $(tabs);
    this.container = this.tabs.closest('.js-container');
    this.contents = this.container.find('.js-products');
    this.tabSlides = this.tabs.find('.swiper-slide');
    this.mobTabSlider;
    this.mobContentSlider;
  }

  init() {
    if (!this.mobTabSlider && !this.mobContentSlider) {
      console.log('init mob sliders!!!');

      this.mobTabSlider = new Swiper(this.tabs[0], {
        slidesPerView: 'auto',
        centeredSlides: true,
        freeMode: true
      });

      this.mobContentSlider = new Swiper(this.contents[0], {
        speed: 500
      });

      this.mobContentSlider.on('slideChange', () => {
        this.active = this.mobContentSlider.activeIndex;
        this.tabSlides.each((index, el) => {
          $(el).removeClass('swiper-slide-active swiper-slide-next swiper-slide-prev');
          $(el).find('.sg-line').removeClass('is-active');
        });
        $(this.tabSlides[this.active]).find('.sg-line').addClass('is-active');
        this.mobTabSlider.slideTo(this.active);
      });


      this.tabSlides.on('click', e => {
        e.preventDefault();
        this.tabSlides.each((index, el) => {
          $(el).removeClass('swiper-slide-active swiper-slide-next swiper-slide-prev');
          $(el).find('.sg-line').removeClass('is-active');
        });
        this.active = $(e.currentTarget).index();
        let activeSlide = $(this.tabSlides[this.active]);
        activeSlide.find('.sg-line').addClass('is-active');
        this.mobContentSlider.slideTo(this.active);
        this.mobTabSlider.slideTo(this.active);
      });

    }
  }

  destroy() {
    if (this.mobTabSlider && this.mobContentSlider) {
      console.log('destroy mob sliders!!!');
      this.mobTabSlider.destroy(true, true);
      this.mobContentSlider.destroy(true, true);

      this.mobTabSlider = undefined;
      this.mobContentSlider = undefined;
    }
  }



}
