import ScrollSection from './ScrollSection';
import ScrollBlock from './ScrollBlock';
import ScrollContainer from './ScrollContainer';
import MobTabs from './MobTabs';

import ScrollMagic from 'scrollmagic';
import { throttle, debounce } from 'throttle-debounce';
import { g } from './../_global';


export default function main() {
  let scrollBlockComponent;
  let scrollSections = [];
  let scrollContainers = [];
  let mobTabsComponents = [];

  $(window).on('load', () => {

    if (window.matchMedia('(min-width: 960px)').matches) {
      init();
    }

    createMobTabs();
    if (!window.matchMedia('(min-width: 960px)').matches) {
      initMob();
    }

    // console.log(g.ipad);
    // console.log(g.ie);


    // window.addEventListener('resize', throttle(200, () => {
    //   if (!g.ipad && window.matchMedia('(min-width: 960px)').matches) {
    //     console.log(location);
    //     setTimeout(() => {
    //       location.reload();
    //     }, 0);
    //   }
      // g.isResizing = true;
      // if (window.matchMedia('(min-width: 960px)').matches) {

      //   destroyMob();

      //   if (!scrollBlockComponent) {
      //     init();
      //   }
      //   else {
      //     update();
      //   }
      // }
      // else {
      //   destroy();
      //   initMob();
      // }
    // }));

    // window.addEventListener('resize', debounce(350, () => {
    //   g.isResizing = false;
    //   console.log(g.isResizing);
    // }));

    window.addEventListener('orientationchange', debounce(300, e => {
      if (window.matchMedia('(min-width: 960px)').matches) {
        destroyMob();

        if (!scrollBlockComponent) {
          init();
        }
        else {
          update();
        }
      }
      else {
        destroy();
        initMob();
      }
    }));

  });

  function init() {
    const scrollBlockEl = $('.js-scroll-block');
    const scrollSectionEl = $('.js-section');
    const scrollContainerEl = $('.js-container');


    if (scrollBlockEl.length !== 0) {
      g.controller = new ScrollMagic.Controller();
      g.innerController = new ScrollMagic.Controller();
      scrollBlockComponent = new ScrollBlock(scrollBlockEl);
    }

    if (scrollSectionEl.length !== 0) {
      scrollSectionEl.each((index, section) => {
        scrollSections.push(new ScrollSection(section));
      });
    }

    if (scrollContainerEl.length !== 0) {
      scrollContainerEl.each((index, section) => {
        scrollContainers.push(new ScrollContainer(section));
      });
    }
  }

  function update() {

    if (g.controller && g.innerController) {
      g.controller = g.controller.destroy(true);
      g.innerController = g.innerController.destroy(true);
    }

    g.controller = new ScrollMagic.Controller();
    g.innerController = new ScrollMagic.Controller();

    if (scrollBlockComponent) scrollBlockComponent.update();

    if (scrollSections.length !== 0) {
      scrollSections.forEach((section) => {
        section.update();
      });
    }

    if (scrollContainers.length !== 0) {
      scrollContainers.forEach((container) => {
        container.update();
      });
    }
  }

  function destroy() {
    if (g.controller && g.innerController) {
      g.controller = g.controller.destroy(true);
      g.innerController = g.innerController.destroy(true);
    }

    scrollBlockComponent = null;
    scrollSections = [];
    scrollContainers = [];

    $('.js-pic')
      .removeAttr('style')
      .removeClass('is-active');

    $('.js-pic')
      .parent()
      .removeAttr('style');

    $('.js-product-nav')
      .removeClass('is-active')
      .parent()
      .removeClass('is-active');
  }


  // mobile scripts
  function createMobTabs() {
    const mobTabsElems = $('.js-mobtabs');

    if (mobTabsElems.length !== 0) {
      mobTabsElems.each((index, el) => {
        mobTabsComponents.push(new MobTabs(el));
      });
    }
  }
  function initMob() {
    if (mobTabsComponents.length !== 0) {
      mobTabsComponents.forEach(el => {
        el.init();
      });
    }
  }
  function destroyMob() {
    if (mobTabsComponents.length !== 0) {
      mobTabsComponents.forEach(el => {
        el.destroy();
      });
    }
  }


}

main();
