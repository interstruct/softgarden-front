import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js';

import { g } from './../_global';

export default class ScrollContainer {
  constructor(section) {
    this.section = $(section);
    this.currentSection = this.section.index();

    this.windowHeight = $(window).height();

    this._init();
  }

  update() {
    this._init();
  }

  _init() {
    this._count();
    this._createScene();
    this._scrollController();
  }

  _count() {
    this.sectionHeight = this.section.outerHeight();
    this.sectionHook = ($('header').outerHeight() + $('.js-product-nav').outerHeight())/this.windowHeight;
    this.topSectionsIndex = 0.2*this.windowHeight;
  }


  _createScene() {
    this.sectionScene = new ScrollMagic.Scene({
      triggerElement: this.section[0],
      duration: this.sectionHeight,
      triggerHook: this.sectionHook,
      offset: -this.topSectionsIndex
    })
      .on('enter', e => {
        this._changeDropdownState();
        this._changeAnchorListState();
        g.currentSection = this.currentSection;
      })
      // .addIndicators({
      //   colorTrigger: '#000000',
      //   colorStart: '#000000',
      //   colorEnd: '#000000'
      // })
      .addTo(g.controller);
  }

  _changeDropdownState() {
    const dropdownItem = $('.js-dropdown-list li');
    const dropdownLink = $(dropdownItem[this.currentSection]).find('a');
    const dropdownTrigger = $('.js-dropdown-trigger a');
    const trinityPath = $('.js-dropdown .js-trinity path');
    // const trinityPathList = $(trinityPath[this.currentSection]);
    let currentHTML = dropdownLink.html();

    if (!g.isDropdownClicked) {
      dropdownTrigger.html(currentHTML);
      dropdownItem.find('a').removeClass('is-active');
      if (!dropdownLink.hasClass('is-active')) dropdownLink.addClass('is-active');
      // trinityPath.removeClass('is-active');
      // if (!trinityPathList.hasClass('is-active')) trinityPathList.addClass('is-active');

      // small hack to add class to svg path
      trinityPath.each((index, path) => {
        path.setAttribute('class', 'trinity__color');
        if (index === this.currentSection) {
          path.setAttribute('class', 'trinity__color is-active');
        }
      });
    }
  }

  _changeAnchorListState() {
    const anchors = $('.js-anchors');
    const achorList = $(anchors[this.currentSection]);
    if (!g.isDropdownClicked) {
      anchors.removeClass('is-active');
      if (!achorList.hasClass('is-active')) achorList.addClass('is-active');
    }
  }


  _scrollController() {
    g.controller.scrollTo((newpos) => {
      let scrollTop = newpos - ($('header').outerHeight()+$('.js-product-nav').outerHeight()) + 1;

      $('html, body').animate({scrollTop: scrollTop}, 800, () => {
        g.isDropdownClicked = false;
        g.scrollToValue = scrollTop;
      });
    });
  }
}

