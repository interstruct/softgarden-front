import ScrollMagic from 'scrollmagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js';

import { g } from './../_global';

export default class ScrollSection {
  constructor(section) {
    this.section = $(section);
    this.pic = this.section.find('.js-pic');
    this.img = this.section.find('.js-pic img');
    this.product = this.section.find('.js-product');
    // this.products = [];

    this.windowHeight = $(window).height();

    this.isUnfixed = false;

    this._init();
    this._scrollInnerController();

  }

  update() {
    this._count();
    this._addParentDimensions();
    this._createSectionScene();
    this._createProductScene();
    this._scrollInnerController();
  }

  _init() {
    if (!g.ie && !g.ipad) {
      this._appendFakePic();
    }
    this._count();
    // this._createProducts();
    if (!g.ie && !g.ipad) {
      this._showFirstPic();
      this._addParentDimensions();
    }
    this._createSectionScene();
    this._createProductScene();
  }


  // _createProducts() {
  //   this.product.each((index, el) => {
  //     if (index !== this.product.length-1) this.products.push(el);
  //   });
  // }

  _appendFakePic() {
    this.fakePic = this.pic.first().clone();
    this.fakePic
      .removeClass('js-pic')
      .addClass('is-fake');
    this.pic.first().parent().append(this.fakePic);
  }

  _showFirstPic() {
    this.pic
      .first()
      .addClass('is-active');
  }

  _count() {
    if (!g.ie && !g.ipad) {
      this.picHeight = this.fakePic.height();
      this.picLeft = this.fakePic.offset().left;
    }
    else {
      this.picHeight = this.pic.height();
      this.picLeft = this.pic.offset().left;
    }

    this.sectionHeight = this.section.height();
    this.productHeight = this.product.outerHeight();
    this.headerHeight = $('header').outerHeight();
    this.navHeight = $('.js-product-nav').outerHeight();
    this.picOffset = this.picHeight/2 - (this.navHeight+this.headerHeight)/2;
    this.sectionOffset = this.productHeight/2 - (this.navHeight+this.headerHeight)/2;
    this.duration = this.sectionHeight - this.productHeight;
  }



  _addParentDimensions() {
    this.pic
      .parent()
      .css({
        'height': this.img.height(),
        'min-width': this.img.width()
      });
  }


  _createProductScene() {
    $(this.product).each((index, el) => {

      let productScene = new ScrollMagic.Scene({
        triggerElement: el,
        duration: this.productHeight,
        triggerHook: 'onCenter'
      })
        .on('enter', e => {
          this.currentElement = index;


          if (!g.ie && !g.ipad) {
            this._makePicInactive();
            this._makePicActive(this.currentElement);
          }

          this._changeAnchorState();
          this._changeUrl();
        })
        // .addIndicators({
        //   colorStart: '#00ffff',
        //   colorEnd: '#00ffff',
        //   colorTrigger: '#00ffff',
        //   indent: 300
        // })
        .addTo(g.innerController);

    });
  }


  _createSectionScene() {
    this.sectionScene = new ScrollMagic.Scene({
      triggerElement: this.section[0],
      duration: this.duration,
      triggerHook: 'onCenter',
      offset: this.sectionOffset
    })
      .on('leave', e => {
        if (!g.ie && !g.ipad) {
          if (e.scrollDirection === 'FORWARD') {
            this._makePicUnfixed(this.currentElement);
          }

          if (e.scrollDirection !== 'FORWARD') {
            this.currentElement !== 0 ?
              this._makePicUnfixed(this.currentElement-1) :
              this._makePicUnfixed(0);
          }
        }

        this._changeAnchorState();
        this._changeUrl();

      })
      .on('enter', e => {
        console.log(g.isResizing);
        console.log(this.currentElement, this.product.length);
        if (!g.ie && !g.ipad) {
          this._makePicFixed();
          // if (!g.isResizing) {
          //   this._makePicFixed();
          // }
          // else {
          //   if (!this.isUnfixed) {
          //     this._makePicFixed();
          //   }
          // }
        }

        // console.log(g.isResizing);

        this._changeAnchorState();
        this._changeUrl();
      })
      // .addIndicators({
      //   indent: 100
      // })
      .addTo(g.innerController);
  }

  _makePicActive(index) {
    if (!$(this.pic[index]).hasClass('is-active')) $(this.pic[index]).addClass('is-active');
  }
  _makePicInactive() {
    if (this.pic.hasClass('is-active')) this.pic.removeClass('is-active');
  }

  _makePicFixed() {
    this.isUnfixed = false;
    this.pic.css({
      'position': 'fixed',
      'left': this.picLeft,
      'top': this.windowHeight*0.5 - this.picOffset
    });
  }

  _makePicUnfixed(index) {
    this.isUnfixed = true;
    $(this.pic[index]).removeAttr('style');
    setTimeout(() => {
      this.pic.removeAttr('style');
    }, 350);
  }

  _changeAnchorState() {
    const anchors = $('.js-anchors');
    const anchor = $(anchors[g.currentSection]).find('.js-anchor');

    if (!g.isAnchorClicked && !g.isDropdownClicked) {
      anchor.removeClass('is-active');
      if (!$(anchor[this.currentElement]).hasClass('is-active')) $(anchor[this.currentElement]).addClass('is-active');

      g.scrollToValue = $(window).scrollTop();
    }
  }

  _changeUrl() {
    const anchors = $('.js-anchors');
    const anchor = $(anchors[g.currentSection]).find('.js-anchor');
    const currentUrl = $(anchor[this.currentElement]).attr('href');
    // If supported by the browser we can also update the URL
    if (!g.isAnchorClicked && !g.isDropdownClicked && window.history && window.history.pushState) {
      history.pushState('', document.title, currentUrl);
    }
  }

  _scrollInnerController() {
    g.innerController.scrollTo((pos) => {
      let offset = (this.productHeight - this.windowHeight)/2 - (this.navHeight+this.headerHeight)/2;
      let scrollTop = pos + offset;

      console.log(g.scrollToValue);
      // count scroll direction on anchors clicks
      let scrollDirection;
      g.scrollToValue < scrollTop ? scrollDirection = 1 : scrollDirection = -1;
      $('html, body').animate({scrollTop: scrollTop + (1*scrollDirection)}, 400, () => {
        g.isAnchorClicked = false;
        g.scrollToValue = scrollTop;
      });

    });
  }

}

