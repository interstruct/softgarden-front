import ScrollSection from './ScrollSection';
import ScrollBlock from './ScrollBlock';
import ScrollContainer from './ScrollContainer';
import MobTabs from './MobTabs';
import {hero} from '../hero/Hero';

import ScrollMagic from 'scrollmagic';
import { throttle, debounce } from 'throttle-debounce';
import { g } from './../_global';


export default function main() {
  let scrollBlockComponent;
  let scrollSections = [];
  let scrollContainers = [];
  let mobTabsComponents = [];

  let distance = $(window).height()/2;

  $(window).on('load', () => {
    let minTouchW = 1260;
    g.isDesktop === true && window.matchMedia(`(min-width: ${minTouchW}px)`).matches ? distance = $(window).height()/2 : distance = 0;

    addTrinityScroll();


    if (window.matchMedia('(min-width: 960px)').matches) {
      init();
    }

    createMobTabs();
    if (!window.matchMedia('(min-width: 960px)').matches) {
      initMob();
    }

    window.addEventListener('resize', throttle(200, () => {
      if (g.isDesktop === true && window.matchMedia(`(min-width: ${minTouchW}px)`).matches) {
        distance = $(window).height()/2;
        if(hero.hero[0]) {
          console.log(!g.heroPlayed);
          if($(window).scrollTop()<500 && !g.heroPlayed) {
            window.scrollTo( 0, 0 );
            hero._recalculateHero();
            hero._go();
            g.heroPlayed = true;
          }
          if($(window).scrollTop()>500 && !g.heroPlayed) {
            hero._recalculateHero();
            hero._hiddenPlay();
            hero._endPlay();
            $('.js-hero-bg-inner').find('img').addClass('is-visible');
            g.heroPlayed = true;
          }
        }
      }

      distance = 0;
      g.isResizing = true;
      if (window.matchMedia('(min-width: 960px)').matches) {

        destroyMob();

        if (!scrollBlockComponent) {
          init();
        }
        else {
          update();
        }
      }
      else {
        destroy();
        initMob();
      }
    }));

    window.addEventListener('orientationchange', debounce(350, e => {
      if(window.matchMedia('(min-width: 1260px)').matches && g.isDesktop === false && hero.hero[0]) {
        if(g.heroPlayed === true) return;
        hero._recalculateHero();
        hero._hiddenPlay();
        $('.js-hero-bg-inner').find('img').addClass('is-visible');
        g.heroPlayed = true;
      }

      if (window.matchMedia('(min-width: 960px)').matches) {
        destroyMob();

        if (!scrollBlockComponent) {
          init();
        }
        else {
          update();
        }
      }
      else {
        destroy();
        initMob();
      }
    }));

  });

  function init() {
    const scrollBlockEl = $('.js-scroll-block');
    const scrollSectionEl = $('.js-section');
    const scrollContainerEl = $('.js-container');


    if (scrollBlockEl.length !== 0) {
      g.controller = new ScrollMagic.Controller();
      g.innerController = new ScrollMagic.Controller();
      scrollBlockComponent = new ScrollBlock(scrollBlockEl);
    }

    if (scrollSectionEl.length !== 0) {
      scrollSectionEl.each((index, section) => {
        scrollSections.push(new ScrollSection(section));
      });
    }

    if (scrollContainerEl.length !== 0) {
      scrollContainerEl.each((index, section) => {
        scrollContainers.push(new ScrollContainer(section));
      });
    }
  }

  function update() {

    if (g.controller && g.innerController) {
      g.controller = g.controller.destroy(true);
      g.innerController = g.innerController.destroy(true);
    }

    g.controller = new ScrollMagic.Controller();
    g.innerController = new ScrollMagic.Controller();

    if (scrollBlockComponent) scrollBlockComponent.update();

    if (scrollSections.length !== 0) {
      scrollSections.forEach((section) => {
        section.update();
      });
    }

    if (scrollContainers.length !== 0) {
      scrollContainers.forEach((container) => {
        container.update();
      });
    }
  }

  function destroy() {
    if (g.controller && g.innerController) {
      g.controller = g.controller.destroy(true);
      g.innerController = g.innerController.destroy(true);
    }

    scrollBlockComponent = null;
    scrollSections = [];
    scrollContainers = [];

    $('.js-pic')
      .removeAttr('style')
      .removeClass('is-active');

    $('.js-pic')
      .parent()
      .removeAttr('style');

    $('.js-product-nav')
      .removeClass('is-active')
      .parent()
      .removeClass('is-active');
  }


  // mobile scripts
  function createMobTabs() {
    const mobTabsElems = $('.js-mobtabs');

    if (mobTabsElems.length !== 0) {
      mobTabsElems.each((index, el) => {
        mobTabsComponents.push(new MobTabs(el, g.activeAnchor));
      });
    }
  }
  function initMob() {
    if (mobTabsComponents.length !== 0) {
      mobTabsComponents.forEach(el => {
        el.init();
      });
    }
  }
  function destroyMob() {
    if (mobTabsComponents.length !== 0) {
      mobTabsComponents.forEach(el => {
        el.destroy();
      });
    }
  }


  function addTrinityScroll() {
    $('.js-scroll-block .js-trinity').on('click', e => {
      e.preventDefault();

      g.scrolling = true;
      $('html, body').animate({
        scrollTop: distance
      }, 800, () => {
        g.scrolling = false;
      });
    });
  }


}

main();
