import ScrollMagic from 'scrollmagic';

import { g } from './../_global';

export default class ScrollBlock {
  constructor(block) {
    this.block = block;
    this.windowHeight = $(window).height();
    this._init();
  }

  update() {
    this._init();
  }

  _init() {
    this._count();
    this._createScene();
  }

  _count() {
    this.blockHeight = this.block.outerHeight();
    this.blockHook = ($('header').outerHeight() + $('.js-product-nav').outerHeight())/this.windowHeight;
    this.topSectionsIndex = $('.js-product-nav').outerHeight();
  }

  _createScene() {
    this.mainScene = new ScrollMagic.Scene({
      triggerElement: this.block[0],
      duration: this.blockHeight,
      triggerHook: this.blockHook,
    })
      .on('leave', e => {
        this._hideNav();
        // this._changeUrl();
      })
      .on('enter', e => {
        this._showNav();
      })
      .addTo(g.controller);
  }

  _hideNav() {
    if ($('.js-product-nav').hasClass('is-active')) {
      $('.js-product-nav')
        .removeClass('is-active')
        .parent()
        .removeClass('is-active');
    }
  }

  _showNav() {
    if (!$('.js-product-nav').hasClass('is-active')) {
      $('.js-product-nav')
        .addClass('is-active')
        .parent()
        .addClass('is-active');
    }
  }

  _changeUrl() {
    // If supported by the browser we can also update the URL
    if (window.history && window.history.pushState) {
      history.pushState('', document.title, ' ');
    }
  }
}

