import { g } from './../_global';

export default function navigation() {
  g.scrolling = false;
  $('.js-dropdown-trigger, .js-dropdown-open .js-dropdown-list').on('mouseover', e => {
    e.preventDefault();
    if(g.isDesktop === false) {
      if(g.scrolling !== false) return;

    }
    const target = $(e.target);
    const dropdown = target.closest('.js-dropdown');
    dropdown.addClass('is-open');
  });

  $('body').on('mouseover', e => {
    e.preventDefault();
    if(g.isDesktop === false) return;
    const target = $(e.target);
    const dropdown = target.closest('.js-dropdown');
    if (dropdown.length !== 0) return;
    $('.js-dropdown').removeClass('is-open');
  });

  $('body').on('touchstart', e => {
    const target = $(e.target);
    const dropdown = target.closest('.js-dropdown');
    if (dropdown.length !== 0) {
      $('.js-dropdown').addClass('is-open');
      return;
    }
    $('.js-dropdown').removeClass('is-open');
  });

  $('.js-dropdown-trigger a').on('click', e => {
    e.preventDefault();
  });

  $('.js-dropdown-list a').on('click', e => {
    e.preventDefault();
    g.isDropdownClicked = true;
    g.scrolling = true;

    const target = $(e.currentTarget);
    const dropdown = target.closest('.js-dropdown');
    const btn = dropdown.find('.js-dropdown-trigger a');
    const targetPath = dropdown.find('.js-trinity path');

    const targetHTML = target.html();
    const targetIndex = target.parent().index();

    const targetSection = target.attr('href');

    const anchors = $('.js-anchors');

    // g.boxController.scrollTo(targetSection);
    g.controller.scrollTo(targetSection);


    // $(targetPath).removeClass('is-active');
    // $(targetPath[targetIndex]).addClass('is-active');

    // small hack to add class to svg path
    targetPath.each((index, path) => {
      path.setAttribute('class', 'trinity__color');
      if (index === targetIndex) {
        path.setAttribute('class', 'trinity__color is-active');
      }
    });

    $('.js-dropdown-list a').removeClass('is-active');
    target.addClass('is-active');

    btn.html(targetHTML);

    anchors.removeClass('is-active');
    $(anchors[targetIndex]).addClass('is-active');
    $(anchors[targetIndex]).find('.js-anchor').removeClass('is-active');
    $(anchors[targetIndex]).find('.js-anchor').first().addClass('is-active');

    const targetAnchorHref = $(anchors[targetIndex]).find('.js-anchor').first().attr('href');

    // If supported by the browser we can also update the URL
    // if (window.history && window.history.pushState) {
    //   history.pushState('', document.title, targetAnchorHref);
    // }

    dropdown.removeClass('is-open');
  });

  $('.js-anchor').on('click', e => {
    g.isAnchorClicked = true;

    e.preventDefault();
    const target = $(e.currentTarget);
    g.activeAnchor = target.index();
    console.log(g.activeAnchor);
    const targetSection = target.attr('href');

    target.closest('.js-anchors').find('.js-anchor').removeClass('is-active');
    g.currentProduct = target.closest('.js-anchors').index();
    console.log(g.currentProduct);
    if (!target.hasClass('is-active')) target.addClass('is-active');

    g.innerController.scrollTo(targetSection);

    // If supported by the browser we can also update the URL
    // if (window.history && window.history.pushState) {
    //   history.pushState('', document.title, targetSection);
    // }
  });
}

navigation();
