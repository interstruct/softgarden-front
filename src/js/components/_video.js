import { g } from './../_global';

export default function video() {

  $(window).on('load', () => {
    let video  = $('#hero-background-video');
    if(video[0]) {
      if(g.isDesktop === false) return;
      g.videoBg = video;
      const videoSrc = g.videoBg.find('source');

      setTimeout(() => {
        addSource(g.videoBg, videoSrc);
      }, 1500);

      if(g.deviceInfo.browser === 'Firefox') {
        video[0].load();
      }
    }
  });

  function addSource(videoEl, srcEl) {
    const src = srcEl.data('src');
    srcEl.attr('src', src);
    videoEl.attr('src', src);

  }
}

video();
