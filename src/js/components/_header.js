import { g } from './../_global';
import menuAim from './../lib/jquery.menu-aim';


export default function header() {
  let isLoaded;
  let isScrolledFromTop;
  let $nav = $('.js-sg-header-nav');
  let aim;

  function stopBodyScrolling(bool) {
    if (bool === true) {
      document.body.addEventListener('touchmove', freezeVp, false);
    } else {
      document.body.removeEventListener('touchmove', freezeVp, false);
    }
  }

  var freezeVp = function(e) {
    e.preventDefault();

  };


  const onNavOpen = e => {
    e.preventDefault();
    e.stopPropagation();
    $(e.currentTarget).toggleClass('is-active');
    $('body').toggleClass('is-nav-open');
    if ($('body').hasClass('is-nav-open')) {
      // if(g.deviceInfo.device !== 'iPad' || g.deviceInfo.device !== 'iPhone') {
      //   $('body').toggleClass('ios-fixed-body');
      //   isScrolledFromTop = $(window).scrollTop();
      //   $('body').css({'top': -isScrolledFromTop});


      // }
      // $nav.css('height', $(window).height());
      $('.js-nav-open').addClass('is-active');
      // fix ios scroll
      // $(document.documentElement).css('overflow', 'hidden');
    }
    else {
      // $nav.removeAttr('style');
      // if(g.deviceInfo.device !== 'iPad' || g.deviceInfo.device !== 'iPhone') {
      //   $('html, body').animate({
      //     scrollTop: isScrolledFromTop
      //   }, 0);
      //   $('body').toggleClass('ios-fixed-body');
      //   $('body').removeAttr('style');

      // }
      $('.js-nav-open').removeClass('is-active');
      // $(document.documentElement).css('overflow', 'visible');

    }

  };


  const closeSubmenu = e => {
    const isHeader = $(e.target).closest('.sg-header').length !== 0;
    if (isHeader) return;

    $('.js-nav-item:not(.is-active)').find('.sg-subnav').addClass('is-hidden');
    $('.js-nav-item.is-active').find('.sg-subnav').removeClass('is-hidden');

    if ($('.js-nav-item.is-active').find('.sg-subnav').length === 0) {
      $('header').removeClass('is-subnav-open');
    }
    else {
      $('header').addClass('is-subnav-open');
    }
  };

  function activateSubmenu(col) {
    console.log('activate');
    const currentCol = $(col);
    const submenu = currentCol.find('.sg-subnav');

    $('.sg-subnav').addClass('is-hidden');
    submenu.removeClass('is-hidden');
    if (submenu.length !== 0 && !submenu.hasClass('.sg-subnav_special')) {
      $('header').addClass('is-subnav-open');
    }
    else {
      $('header').removeClass('is-subnav-open');
    }
  }

  function deactivateSubmenu(col) {
    console.log('deactivate');
    const currentCol = $(col);
    const submenu = currentCol.find('.sg-subnav');

    submenu.addClass('is-hidden');
    $('.js-nav-item.is-active').find('.sg-subnav').removeClass('is-hidden');
    if ($('.js-nav-item.is-active').find('.sg-subnav').length === 0) {
      $('header').removeClass('is-subnav-open');
    }
    else {
      $('header').addClass('is-subnav-open');
    }
  }


  $(document).ready(() => {
    if (window.matchMedia('(min-width: 1024px)').matches) {
      $('header').removeClass('is-subnav-open');

      if ($('.sg-nav__item_accordion').hasClass('is-active')) $('header').addClass('is-subnav-open');
      $('.sg-subnav').addClass('is-hidden');
      $('.js-nav-item.is-active').find('.sg-subnav').removeClass('is-hidden');

      $('body').on('mouseover', closeSubmenu);

      $('.sg-nav').menuAim({
        activate: activateSubmenu,
        deactivate: deactivateSubmenu,
        submenuDirection: 'below'
      });
    }
    else {
      console.log('reload mobile header');
      $('.sg-subnav').removeClass('is-hidden');
      $('.js-nav-open').on('click', onNavOpen);
      isLoaded = true;
    }
  });

  let isMobileHeader = true;
  let isDesktopHeader = true;

  window.addEventListener('resize', () => {
    if (window.matchMedia('(min-width: 1024px)').matches) {
      if (isDesktopHeader) {
        // $('header').removeClass('is-subnav-open');

        if ($('.js-nav-item.is-active').find('.sg-subnav').length === 0) {
          $('header').removeClass('is-subnav-open');
        }
        else {
          $('header').addClass('is-subnav-open');
        }
        $('.sg-subnav').addClass('is-hidden');
        $('.js-nav-item.is-active').find('.sg-subnav').removeClass('is-hidden');

        $('body').removeClass('is-nav-open');
        $('.js-nav-open').removeClass('is-active');
        $nav.removeAttr('style');

        $('.js-nav-open').off('click', onNavOpen);

        $('body').on('mouseover', closeSubmenu);

        $('.sg-nav').menuAim({
          activate: activateSubmenu,
          deactivate: deactivateSubmenu,
          submenuDirection: 'below'
        });

        isDesktopHeader = false;
        isMobileHeader = true;
        isLoaded = false;
      }

    }
    else {
      if (isMobileHeader && !isLoaded) {
        console.log('resize mobile header');

        $('body').off('mouseover', closeSubmenu);
        $('.sg-nav').menuAim('destroy');

        $('header').removeClass('is-subnav-open');
        $('.sg-subnav').removeClass('is-hidden');

        $('.js-nav-open').on('click', onNavOpen);

        isMobileHeader = false;
        isDesktopHeader = true;
      }
    }
    if(g.isDesktop === false) {
      // $nav.css('height', $(window).height());
    }
  });

  window.addEventListener('orientationchange', () => {

    if (window.matchMedia('(min-width: 1024px)').matches) {
      if (isDesktopHeader) {
        $('header').removeClass('is-subnav-open');
        $('.sg-subnav').addClass('is-hidden');
        $('.js-nav-item.is-active').find('.sg-subnav').removeClass('is-hidden');

        $('body').removeClass('is-nav-open');
        $('.js-nav-open').removeClass('is-active');
        $nav.removeAttr('style');

        $('.js-nav-open').off('click', onNavOpen);

        $('body').on('mouseover', closeSubmenu);

        $('.sg-nav').menuAim({
          activate: activateSubmenu,
          deactivate: deactivateSubmenu,
          submenuDirection: 'below'
        });

        isDesktopHeader = false;
        isMobileHeader = true;
        isLoaded = false;
      }

    }
    else {


      if (isMobileHeader && !isLoaded) {
        console.log('orientationchange mobile header');
        $('header').removeClass('is-subnav-open');
        $('.sg-subnav').removeClass('is-hidden');
        $('body').off('mouseover', closeSubmenu);

        $('.sg-nav').menuAim('destroy');

        $('.js-nav-open').on('click', onNavOpen);

        isMobileHeader = false;
        isDesktopHeader = true;
      }else{
        if(isMobileHeader && isLoaded) {
          setTimeout(() => {
            // $nav.css('height', $(window).height());
            // fixHeroBg();
          }, 350);
        }
      }
    }
  });

  // function fixHeroBg() {

  //   if(g.deviceInfo.device === 'Android' || g.deviceInfo.device === 'iPhone' || g.deviceInfo.device ==='Windows Phone') {
  //     let orientation = window.matchMedia('(orientation: portrait)').matches ? 'portrait': 'landscape';
  //     let imgSrc;
  //     let height = $('.js-hero').innerHeight();
  //     if(orientation ==='portrait') {
  //       imgSrc = $('.js-hero-bg-inner').find('.sg-bg-video__img_portrait').attr('src');

  //     }else{
  //       imgSrc = $('.js-hero-bg-inner').find('.sg-bg-video__img_landscape').attr('src');
  //     }
  //     console.log(imgSrc);
  //     $('.js-hero-bg').css({'background-image':`url('${imgSrc}')`, 'height': `${height}px`} );
  //   }

  // };
  // fixHeroBg();

  function createMobBodyOverlay() {
    // if(g.isDesktop === false) {
    let overlay = $('<div>');
    overlay.addClass('body-mob-overlay js-body-mob-overlay');
    $('body').prepend(overlay);

    $('.js-body-mob-overlay').on('click', function(e) {
      e.preventDefault();
      onNavOpen(e);
    });
    // }
  }

  function prepareMobMenu() {
    $nav.addClass('is-prepared');
  }

  const mobHeaderPoint = window.matchMedia('(max-width: 1024px)');


  function testHeaderScreen(point) {
    if (point.matches) {
      createMobBodyOverlay();
      prepareMobMenu();
    }
  }

  testHeaderScreen(mobHeaderPoint);
  mobHeaderPoint.addListener(testHeaderScreen);

  // if(g.isDesktop === false) {
  //   createMobBodyOverlay();
  //   prepareMobMenu();
  // }
};

header();
