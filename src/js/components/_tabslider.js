// import Swiper from 'swiper';
import Swiper from 'swiper/dist/js/swiper';

export default function tabSlider() {
  let filterSlider, hasRefSlider, refSlider;
  let active;
  const references = $('.js-references');
  const filters = $('.js-filters');
  const filterSlides = filters.find('.swiper-slide');

  const slideToActive = e => {
    filterSlider.slideTo(active);
  };

  const scrollToTop = e => {
    let pos = filters.offset().top;
    let offset = $('.sg-header').height() - 10;
    // console.log('scroll');
    $('html, body').animate({
      scrollTop: pos - offset
    });
  };

  $(document).ready(() => {

    let trackWidth = 0;

    filterSlides.each((index, el) => {
      $(el).removeClass('is-active swiper-slide-active swiper-slide-next swiper-slide-prev');
    });

    $(filterSlides[0]).addClass('is-active');

    if(references.length !== 0) {

      hasRefSlider = true;

      refSlider = new Swiper(references, {
        speed: 1000,
        slidesPerView: 1,
        pagination: {
          el: '.sg-references__pagination',
          type: 'bullets',
          clickable: true
        },
        navigation: {
          nextEl: '.sg-references__next',
          prevEl: '.sg-references__prev',
        }
      });

      // on slide change event
      refSlider.on('slideChange', () => {
        active = refSlider.activeIndex;
        filterSlides.each((index, el) => {
          $(el).removeClass('is-active swiper-slide-active swiper-slide-next swiper-slide-prev');
        });
        $(filterSlides[active]).addClass('is-active');
      });  

    } else { hasRefSlider = false; }

    filters.find('.swiper-slide').on('click', e => {
      e.preventDefault();
      filterSlides.each((index, el) => {
        $(el).removeClass('is-active swiper-slide-active swiper-slide-next swiper-slide-prev');
      });
      active = $(e.target).closest('.swiper-slide').index();
      let activeSlide = $(filterSlides[active]);
      activeSlide.addClass('is-active');
      if(hasRefSlider) refSlider.slideTo(active);
    });

    if (!window.matchMedia('(min-width: 720px)').matches) {
      filterSlider = new Swiper(filters, {
        slidesPerView: 'auto',
        // centeredSlides: true,
        freeMode: true
      });
      filters.find('.swiper-slide').on('click', slideToActive);
      // filters.find('.swiper-slide').on('click', scrollToTop);
      if(hasRefSlider) refSlider.on('slideChange', slideToActive);
    }
  });

  let isMobileSlider = true;
  let isDesktopSlider = true;

  window.addEventListener('resize', () => {
    if (!window.matchMedia('(min-width: 720px)').matches) {

      if (!filterSlider && isMobileSlider) {
        filterSlider = new Swiper(filters, {
          slidesPerView: 'auto',
          // centeredSlides: true,
          freeMode: true
        });

        filters.find('.swiper-slide').on('click', slideToActive);
        // filters.find('.swiper-slide').on('click', scrollToTop);
        if(hasRefSlider) refSlider.on('slideChange', slideToActive);

        isMobileSlider = false;
        isDesktopSlider = true;
      }

    }
    else {
      if (filterSlider && isDesktopSlider) {
        filters.find('.swiper-slide').off('click', slideToActive);
        // filters.find('.swiper-slide').off('click', scrollToTop);
        if(hasRefSlider) refSlider.off('slideChange', slideToActive);
        filterSlider.destroy(true, true);
        filterSlider = null;

        isDesktopSlider = false;
        isMobileSlider = true;
      }
    }
  });
}

tabSlider();
