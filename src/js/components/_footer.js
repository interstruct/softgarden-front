// import Swiper from 'swiper';
import Swiper from 'swiper/dist/js/swiper';

export default function footer() {
  let swiper, newsbox;

  $(document).ready(() => {
    newsbox = $('.js-newsbox');

    if (!window.matchMedia('(min-width: 568px)').matches) {
      swiper = new Swiper(newsbox, {
        pagination: {
          el: newsbox.find('.swiper-pagination'),
          type: 'bullets',
          clickable: true,
          autoplay: false
        }
      });
    }
  });

  let isMobileSwiper = true;
  let isDesktopSwiper = true;

  $(window).resize(() => {
    if (!window.matchMedia('(min-width: 568px)').matches) {
      if (isMobileSwiper && !swiper) {
        swiper = new Swiper(newsbox, {
          pagination: {
            el: newsbox.find('.swiper-pagination'),
            type: 'bullets',
            clickable: true
          }
        });
        isMobileSwiper = false;
        isDesktopSwiper = true;
      }
    }
    else {
      if (isDesktopSwiper && swiper) {
        swiper.destroy(true, true);
        swiper = null;
        isDesktopSwiper = false;
        isMobileSwiper = true;
      }
    }
  });
}

footer();
