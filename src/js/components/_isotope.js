import isotope from 'isotope-layout';

const isotopeWrapp = $('.js-isotope');

isotopeWrapp.each(( index, item ) => {

  const iso = new isotope(item, {
    filter: 'none',
    itemSelector: '.js-isotope-item',
    percentPosition: true,
    // fitWidth: true,
    fitRows: true,
    masonry: {
	    gutterWidth: '.js-isotope-item',
	    gutter: '.js-isotope-gutter',
    },
  });

  iso.once( 'layoutComplete', () => {
    const $items = $(item).find('.js-isotope-item');
    $(item).addClass('is-showing-items');
    iso.revealItemElements( $items );
    iso.layout();
  });

  const filters = $(`[data-iso-filter="${ $(item).data('filter') }"]`);
  const filterItems = filters.find( '.js-iso-filter-btn' );

  filterItems.each(( index, btn ) => {

  	$(btn).on('click', (e) => {

  		e.preventDefault();

  		// const currentBtn = $(btn);

  		// filterItems.removeClass('is-active');
  		// currentBtn.addClass('is-active');

  		const sortValue = $(btn).data('sortBy');
  		iso.arrange({ filter: sortValue });

  	});

  });

  iso.arrange({ filter: '*' });

});
