const scrollTo = ( url ) => {

  $('html, body').animate({
    	scrollTop: $(`${ url }`).offset().top - $('.sg-header').outerHeight(),
  }, 1500);
};

$('.js-scroll-to').each(( index, item ) => {

  const link = $(item);

  link.on('click', ( e ) => {
  	e.preventDefault();
    scrollTo( $(link).attr('href') );
  });

});
