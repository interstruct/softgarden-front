import { breakpoints } from './../_consts.js';

const isTouch = () => 'ontouchstart' in window;

class Tooltip {

  constructor( wrapp ) {
    this.tooltipWrapp = $(wrapp);
    this.tooltipTitle = $(this.tooltipWrapp.find('[data-tooltip-heading]'));
    this.tooltip = $(this.tooltipWrapp.find('[data-tooltip-text]'));

    this.tooltipItem = this.tooltip.find('.sq-tooltip');

    this.leave = this.leave.bind(this);
    this.show = this.show.bind(this);
    this.updatePosition = this.updatePosition.bind(this);

    this.winWidth = $(window).width();
    this.winHeight = $(window).height();

    this.itemWidth = this.tooltip.outerWidth();
    this.itemHeight = this.tooltip.outerHeight();

    this.wrapperWidth = this.tooltipWrapp.width();
    this.wrapperHeight = this.tooltipWrapp.height();

    this.wrapperPaddingL = parseInt(this.tooltipWrapp.css('padding-left'));

    this.headerHeight = $('.sg-header').outerHeight();

    this.offset = 15;
  }

  updatePosition( e ) {

  	const x = e.pageX - $(e.currentTarget).offset().left;
  	const y = e.pageY - $(e.currentTarget).offset().top;

  	const offsetX = this.winWidth - this.tooltipWrapp.offset().left - x;
  	const offsetY = this.tooltipWrapp.offset().top - $(window).scrollTop() + y;

  	const xDirection = offsetX < this.itemWidth ? 'right' : 'left';
  	const yDirection = offsetY < this.itemHeight + this.headerHeight ? 'top' : 'bottom';

  	const posY = yDirection === 'top' ? y + this.offset : this.wrapperHeight - y + this.offset;
  	const posX = xDirection === 'left' ? x + this.offset : this.wrapperWidth - x + this.offset + this.wrapperPaddingL;

  	this.tooltip.css('top', 'auto').css('left', 'auto').css('right', 'auto').css('bottom', 'auto');
  	this.tooltip.css(yDirection, posY).css(xDirection, posX);

  }

  hideTooltip( item ) {
    const tooltip = item ? item.closest('[data-tooltip-text]') : this.tooltip;
    const tooltipItem = item ? item : this.tooltipItem;

    tooltip.css('display', 'none');
    tooltipItem.removeClass('is-active');
  }

  showTooltip() {
    if(this.tooltipItem.hasClass('is-active')) return;
    this.tooltip.css('display', 'block');
    this.tooltipItem.addClass('is-active');
  }

  show(e) {
    this.showTooltip();
  	this.updatePosition(e);
  }

  leave() {
    this.hideTooltip();
    this.destroy();
  }

  destroy() {
  	this.tooltipWrapp.off('mousemove');
  	this.tooltipWrapp.off('mouseleave');
  }

  onMouseMove() {
    this.tooltipWrapp.on('mousemove', this.show);
    this.tooltipWrapp.on('mouseleave', this.leave);
  }

  onClick(e) {
    this.hideTooltip($('.sq-tooltip.is-active'));
    this.showTooltip();
    this.show({
      pageX: e.pageX,
      pageY: e.pageY,
      currentTarget: this.tooltipWrapp
    });
  }

  init(e) {
    if(this.winWidth < breakpoints.mobile) return;
    isTouch() ? this.onClick(e) : this.onMouseMove();
  }

}

const clickOutside = (e) => {

  const target = e.target;
  const targetTooltip = $(target).closest('[data-tooltip-item]');

  if(targetTooltip.length) return;

  const activeTooltip = $('.sq-tooltip.is-active');
  activeTooltip.closest('[data-tooltip-text]').css('display', 'none');
  activeTooltip.removeClass('is-active');

};

const init = e => {
  const target = $(e.target);
  const targetTooltip = target.closest('[data-tooltip-item]');

  if(!targetTooltip.length) return;
  if(target.closest('[data-tooltip-text]').length) return;
  const tooltip = new Tooltip( targetTooltip ).init(e);
};

$('[data-tooltip-wrapp]').each(( index, item ) => {
  isTouch() ? $(item).on('click', init) : $(item).on('mousemove', init);
});

module.exports = {
  tooltipClickOutside: clickOutside
};
