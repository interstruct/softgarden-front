import { g } from './../_global';
import ScrollMagic from 'scrollmagic';
export default function prodFade() {
  if(g.deviceInfo.device === 'Android'|| g.deviceInfo.device === 'iPad' || g.deviceInfo.device === 'iPhone' || g.deviceInfo.device ==='Windows Phone') return;
  let sectionController = new ScrollMagic.Controller();
  $('.js-product ').each((index, el) => {
    let $img = $(el).find('.js-pic');
    if(!$img[0])return;


    let scene  = new ScrollMagic.Scene({
      triggerElement: el,
      duration: 0.3,
      // triggerHook: this.sectionHook,
      offset: -70
    })
      .on('enter', e => {
        $img.addClass('is-fade-in');
        // g.currentSection = this.currentSection;
      })

      .addTo(sectionController);
    // console.log(g);
  });

}
// if($('.js-product')[0]) {
//   prodFade();
// }

