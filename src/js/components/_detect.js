const deviceDetect = require('device-detect')();
const { detect } = require('detect-browser');
const browser = detect();

import { g } from './../_global';

export default function detectAll() {
  // console.log(deviceDetect, browser);
  g.deviceInfo = deviceDetect;
  if (deviceDetect.device === 'iPad') {
    g.ipad = true;
    $('body').addClass('is-ipad');
  }
  if (browser.name === 'ie') {
    g.ie = true;
    $('body').addClass('is-ie');

  }
  if(g.deviceInfo.device !== 'Android' && g.deviceInfo.device !== 'iPad' && g.deviceInfo.device !== 'iPhone' && g.deviceInfo.device !=='Windows Phone') {
    g.isDesktop = true;
  }else{
    g.isDesktop = false;
    $('body').addClass('is-tablet-or-mob');

  }

  if (browser.name === 'ios' && deviceDetect.browser === 'Safari') $('body').addClass('is-ios-safari');
  if(g.deviceInfo.device === 'Android') {
    $('body').addClass('is-mob-android');
  }
  if(g.deviceInfo.device === 'iPhone') {
    $('body').addClass('is-iphone');
  }
  if(g.deviceInfo.device ==='Windows Phone') {
    $('body').addClass('is-mob-windows');
  }
  if (!g.ipad && !g.ie) return;

  $('body').addClass('is-special');
}

detectAll();
