export default function form() {
  hbspt.forms.create({
    css: '',
    target: '.hbspt-forms-target-8417c715-ac68-4f7e-a992-b56e958f5574',
    groupErrors: true,
    cssClass: 'sg-signup-form',
    portalId: '436029',
    formId: '8417c715-ac68-4f7e-a992-b56e958f5574',
    onFormReady: function(form) {
      manipulateForm(form);
    },
    onFormSubmit: function() {
      //window.dataLayer.push({event: 'hbspt_form_submit'});
    }
  });

  function manipulateForm(form) {
    const fields = $(form).find('.hs-input').filter('[id]').closest('.hs-form-field');

    const fieldObserver = new MutationObserver(mutations => {
      mutations.forEach(mutation => {
        if (mutation.addedNodes.length !== 0) {
          $(mutation.target).addClass('is-error');
        }
        if (mutation.addedNodes.length === 0) {
          $(mutation.target).removeClass('is-error');
        }
      });
    });

    fields.each((index, field) => {
      fieldObserver.observe(field, {
        childList: true
      });
    });

    const inputs = $(form).find('.hs-input').filter('[id]');

    inputs.each((index, input) => {
      $(input).on('blur', e => {
        if ($(e.target).val() !== '') {
          console.log($(e.target).val());
          $(e.target).closest('.hs-form-field').addClass('is-completed');
        }
        else {
          console.log('no value');
          $(e.target).closest('.hs-form-field').removeClass('is-completed');
        }
      });
    });
  }
}

form();
