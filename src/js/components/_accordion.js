export default function accordion() {
  let isLoaded, isOpening;

  const onAccordionClick = e => {
    const trigger = $(e.target);
    const wrapper = trigger.closest('.js-accordion');
    const inner = wrapper.find('.js-accordion-in');
    const visibleElements = $('.js-accordion-in').filter(':visible');

    if (!isOpening) {
      isOpening = true;
      wrapper.toggleClass('is-open');
      inner.slideToggle(300, function() {
        isOpening = false;
      });
    }

    $('.js-accordion').not(wrapper).removeClass('is-open');
    $('.js-accordion-in').not(inner).slideUp(300);
    return false;
  };

  $(document).ready(() => {
    if (window.matchMedia('(min-width: 1024px)').matches) return;
    $('.js-accordion-in').removeAttr('style');
    $('.js-accordion.is-active').addClass('is-open');
    $('.js-accordion.is-active .js-accordion-in').show();
    $('.js-accordion-trigger').on('click', onAccordionClick);
    isLoaded = true;
    console.log('reload mobile');
  });

  let isMobileAccordion = true;
  let isDesktopAccordion = true;

  window.addEventListener('resize', () => {
    if (window.matchMedia('(min-width: 1024px)').matches) {
      if (isDesktopAccordion) {
        $('.js-accordion').removeClass('is-open');

        $('.js-accordion-in').removeAttr('style');
        $('.js-accordion-in').addClass('is-hidden');
        $('.js-accordion.is-active').find('.js-accordion-in').removeClass('is-hidden');

        $('.js-accordion-trigger').off('click', onAccordionClick);
        isDesktopAccordion = false;
        isMobileAccordion = true;
        isLoaded = false;
      }
    }
    else {
      if (isMobileAccordion && !isLoaded) {
        $('.js-accordion').removeClass('is-open');
        $('.js-accordion-in').removeAttr('style');
        $('.js-accordion.is-active').addClass('is-open');
        $('.js-accordion.is-active .js-accordion-in').show();
        $('.js-accordion-trigger').on('click', onAccordionClick);
        console.log('resize mobile');
        isMobileAccordion = false;
        isDesktopAccordion = true;
      }
    }

  });

  window.addEventListener('orientationchange', () => {
    if (window.matchMedia('(min-width: 1024px)').matches) {
      if (isDesktopAccordion) {
        $('.js-accordion').removeClass('is-open');

        $('.js-accordion-in').removeAttr('style');
        $('.js-accordion-in').addClass('is-hidden');
        $('.js-accordion.is-active').find('.js-accordion-in').removeClass('is-hidden');

        $('.js-accordion-trigger').off('click', onAccordionClick);
        isDesktopAccordion = false;
        isMobileAccordion = true;
        isLoaded = false;
      }
    }
    else {
      if (isMobileAccordion && !isLoaded) {
        console.log('orientationchange mobile');
        $('.js-accordion').removeClass('is-open');
        $('.js-accordion-in').removeAttr('style');
        $('.js-accordion.is-active').addClass('is-open');
        $('.js-accordion.is-active .js-accordion-in').show();
        $('.js-accordion-trigger').on('click', onAccordionClick);
        isMobileAccordion = false;
        isDesktopAccordion = true;
      }
    }

  });
}

accordion();


