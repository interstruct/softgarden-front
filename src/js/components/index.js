import './_detect';
import './_accordion';
import './_header';
import './_footer';
import './_tabslider';
import './_navigation';
import './_video';
import './_prodSectionFade';
import './_isotope';
import { tooltipClickOutside } from './_tooltip';
import './_scroll-to';

$('body').on('click touchstart', (e) => {
  tooltipClickOutside(e);
});
