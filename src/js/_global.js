class Global {
  constructor() {
    this.isDropdownClicked = false;
    this.isAnchorClicked = false;

    this.activeAnchor = 0;
    this.currentProduct = 0;

    this.scrollToValue = 1;

    this.ipad = false;
    this.ie = false;
    this.devInfo;
    this.isResizing = false;
    this.heroPlayed = false;
  }
}

export let g = new Global();
