import ScrollMagic from 'scrollmagic';
import { throttle } from 'throttle-debounce';

import {hero} from './Hero';
import navigateToProdSections from './_navigationToProdSections';
import { g } from './../_global';

function heroMain() {

  g.heroInnerController = new ScrollMagic.Controller();
  $(document).ready(() => {
    if (window.matchMedia('(min-width: 1260px)').matches) {
      $('.js-hero-link').off('click', g.navigateMobileHero);
      const heroEl = $('.js-hero');

      if (heroEl.length !== 0) {
        hero._go();
        g.heroPlayed = true;
      }
    }
    else {
      navigateToProdSections();
    }
  });

  // fix fixed hero
  if(g.ie) {
    $('body').on('mousewheel', function() {

      event.preventDefault();

      var wheelDelta = event.wheelDelta;
      var currentScrollPosition = window.pageYOffset;
      window.scrollTo(0, currentScrollPosition - wheelDelta);
    });
  }


  if(g.deviceInfo.device === 'iPhone' || g.ipad) {
    $(window).on('scroll', function(e) {
      let docH = $(document).outerHeight(true)/2;
      let scrollPos = $(this).scrollTop();
      if (scrollPos > docH) {
        $('.js-hero-bg').css('position', 'absolute');
      } else {
        $('.js-hero-bg').css('position', 'fixed');
      }
    });
  }
}

heroMain();

