import { g } from './../_global';

export default function navigateToProdSections() {
  const link = $('.js-hero-link');

  g.navigateMobileHero = e => {
    e.preventDefault();
    const href = $(e.currentTarget).attr('href');
    console.log(href);

    $('html, body').animate({
      scrollTop: $(href).offset().top - 32
    }, 600);
  };

  link.on('click', g.navigateMobileHero);
}
