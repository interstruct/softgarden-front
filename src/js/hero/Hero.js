import ScrollMagic from 'scrollmagic';
import { TimelineLite } from 'gsap/TweenMax';
import { throttle, debounce } from 'throttle-debounce';
// import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js';

import { g } from './../_global';

class Hero {
  constructor(hero) {
    this.hero = hero;
    this.bg = this.hero.find('.js-hero-bg');
    this.trinity = this.hero.find('.js-hero-trinity');
    this.svg = this.hero.find('.sg-trinity');
    this.lines = this.trinity.find('.js-lines path');
    this.circles = this.trinity.find('.js-circles circle');
    this.icons = this.trinity.find('.js-icons path');

    this.part = this.hero.find('.js-hero-part');
    this.text = this.hero.find('.js-hero-text');
    this.links = this.hero.find('.js-hero-link');
    this.linkLines = this.hero.find('.js-hero-link-line');

    this.heroOuterAnchor = this.hero.find('.js-anchor-outer');

    this.isAnimating = false;
    this.isReverse = false;

    this.windowHeight = $(window).height();
    this.tlStart = new TimelineLite({paused: true});
    this.tlEnd = new TimelineLite({paused: true});

    this.triggerOffset = 0.38;
    this.played = false;
    this.animated = false;
    this.trinityBreakPoint = this.hero.innerHeight()*0.68;

  }

  _go() {
    this._init();
  }

  _recalculateHero() {
    this.trinityBreakPoint = this.hero.innerHeight()*0.68;
  }


  _init() {
    if(!this.hero[0]) return;
    this._count();
    this._scrollBehaviour();

    setTimeout(() => {
      this._createOuterScene();
      this._createInnerScene();

      // this._scrollOuterController();
      this._scrollInnerController();
      this._addHeroClicks();
    }, 10);
  }
  _scrollBehaviour() {
    let t = this.hero[0].getBoundingClientRect();
    window.onbeforeunload = function() {
      let localData = $('.js-hero')[0].getBoundingClientRect();
      if(localData.bottom >= 0) {
        window.scrollTo(0,0);
      }

    };

    if(t.bottom >= 0) {
      setTimeout(() => {
        if(g.deviceInfo.browser === 'ie') {

        }else{
          this._start();
        }
      },0);

    }else{
      this._hiddenPlay();
    }
  }

  _count() {
    this.bgHeight = this.bg.outerHeight();
    this.padding = $('header').outerHeight();
  }
  _hiddenPlay() {
    this.trinity.css({
      'position': 'absolute',
      'top': `${this.trinityBreakPoint*0.9}px`
    });
    this.played = true;
    this.tlStart
      .to(this.svg, 0, {opacity: 1}, 0)
      .to(this.part, 0, {opacity: 1}, 0)
      .to(this.text, 0, {opacity: 1}, 0)
      .to(this.svg, 0, {rotation: 0}, 0)
      .to(this.circles, 0, {opacity: 1}, 0)
      .to(this.lines, 0, {opacity: 1}, 0)
      .to(this.icons, 0, {opacity: 1}, 0)
      .to(this.links, 0, {opacity: 1, y: 0}, 0)
      .to(this.linkLines, 0, {opacity: 1, y: 0}, 0);
    $('.js-hero').addClass('is-vertical-centered');
    $('.js-hero-forward').addClass('change-view');
    this.tlStart.play();
  }
  _start() {
    this.tlStart
      .fromTo(this.svg, 0.75, {opacity: 0}, {opacity: 1, ease: Power2.easeInOut})
      .fromTo(this.svg, 1.2, {rotation: 0}, {rotation: 360, ease: Power2.easeInOut})
      .staggerTo(this.lines, 0.25, {opacity: 1, ease: Power2.easeInOut}, 0.125, '-=1.5')
      .staggerTo(this.part, 0.75, {opacity: 1, ease: Power2.easeInOut}, 0.375)
      .fromTo(this.text, 0.375, {opacity: 0}, {opacity: 1, ease: Power2.easeInOut, onComplete:function() {
        $('.js-hero-forward').removeClass('is-start-view');
      } }, '-=.375');

    this.tlStart.play();
  }
  _arrowLinkPlay() {
    let link = $('.js-hero-forward');
    let linkTl = new TimelineLite();
    let heroH = this.hero.innerHeight();
    let top = link.position().top + link.innerHeight();
    let toLinkScroll = heroH - top;
    linkTl
      .to(link, 0.2, {opacity: 0, ease: Power2.easeInOut, onComplete: function() {
        link.addClass('change-view');
      }}, 0);
  }
  _endPlay() {
    this._arrowLinkPlay();
    this.tlEnd = null;
    this.tlEnd = new TimelineLite({paused: true});
    this.tlEnd
      .to($('.js-hero-headline'), 0.35, {opacity: 0, ease: Power2.easeInOut}, 0)
      .fromTo(this.svg, 0.6, {rotation: 360}, {rotation: 0, ease: Power2.easeInOut},'-=.35')
      .fromTo(this.lines, 0.375, {opacity: 1}, {opacity: 0.5, ease: Power2.easeInOut}, '-=.375')
      .staggerTo(this.circles, 0.125, {opacity: 1, ease: Power2.easeInOut}, 0.0625)
      .staggerTo(this.icons, 0.125, {opacity: 1, ease: Power2.easeInOut}, 0.0625, '-=.1875')
      .staggerTo(this.links, 0.25, {opacity: 1, y: 0, ease: Power2.easeInOut}, 0.125, '-=.3125')
      .staggerTo(this.linkLines, 0.25, {y: 0, ease: Power2.easeInOut, onComplete: setTextPos}, 0.125, '-=0.825');

    this.tlEnd.play();
    function setTextPos() {
      $('.js-hero').addClass('is-vertical-centered');
    }
  }

  _createOuterScene() {
    this.heroOuterScene = new ScrollMagic.Scene({
      triggerElement: this.hero[0],
      triggerHook: 0,
      duration: this.windowHeight
    })
      .on('enter', e => {

        if(g.videoBg !== undefined) {
          g.videoBg.get(0).play();
          setTimeout(() => {
            $('.js-hero-bg-inner').addClass('change-view');
          }, 2000);
        }else{
          setTimeout(() => {
            $('.js-hero-bg-inner').addClass('change-view');
          }, 2000);
        }
      })
      .addTo(g.heroInnerController);
  }

  _createInnerScene() {
    this.heroInnerScene = new ScrollMagic.Scene({
      triggerElement: this.hero[0],
      triggerHook: 0,
      duration: this.bgHeight*this.triggerOffset,
      offset: this.padding
    })
      .on('leave', e => {
        if(this.played === true) return;

        if (e.scrollDirection === 'FORWARD') {
          this._makeTrinityStatic();
          this._endPlay();
          this.played = true;
        }
      })
      // .addIndicators({
      //   name: 'hero inner scene',
      //   colorStart: '#d33dda',
      //   colorEnd: '#d33dda',
      //   indent: 200
      // })
      .addTo(g.heroInnerController);
  }
  _makeTrinityStatic() {
    // let trinityOffset = this.trinity.offset().top + this.trinity.innerHeight()/2;
    // // let trinityOffset = $(window).scrollTop() + this.trinity.innerHeight()/2;
    // trinityOffset = trinityOffset > this.trinityBreakPoint ? this.trinityBreakPoint : trinityOffset;
    // // trinityOffset = trinityOffset > this.hero.innerHeight() ? this.trinityBreakPoint : trinityOffset;
    // console.log(trinityOffset);
    // this.trinity.css({
    //   'position': 'absolute',
    //   // 'top': `${this.trinityBreakPoint}px`
    //   // 'top': `${trinityOffset}px`
    // });
    this.trinity.addClass('is-below-centered');
    // setTimeout(() => {
    //   this.trinity.addClass('is-below-centered');
    // }, 200);
  }
  _scrollInnerController() {
    g.heroInnerController.scrollTo((newpos) => {
      let scrollTop = newpos;

      $('html, body').animate({scrollTop: newpos}, 800);
    });
  }

  _scrollAnchorLink(e) {
    e.preventDefault();
    if(this.played === false) {
      g.heroInnerController.scrollTo(this.bg.innerHeight()*0.5);
    }else{
      const href = $(e.currentTarget).attr('href');
      g.heroInnerController.scrollTo(href);
    }
  }
  _navigateHero(e) {
    e.preventDefault();
    g.heroInnerController.scrollTo(this.bg.innerHeight()*0.5);
  }

  _scrollToProducts(e) {
    e.preventDefault();
    const href = $(e.currentTarget).attr('href');
    g.heroInnerController.scrollTo(href);
  }

  _scrollToSections(e) {
    e.preventDefault();
    const href = $(e.currentTarget).attr('href');
    g.controller.scrollTo(href);
  }

  _addHeroClicks() {
    this.heroOuterAnchor.on('click', this._scrollAnchorLink.bind(this));
    this.links.on('click', this._scrollToSections.bind(this));
  }
  _scrollTo(newpos) {
    let offset = $(`${newpos}`).offset().top;
    let scrollTop = offset - ($('header').outerHeight()) + 1;

    $('html, body').animate({scrollTop: scrollTop}, 800, () => {
      g.isDropdownClicked = false;
      g.scrollToValue = scrollTop;
    });
  }

}


export let hero = new Hero($('.js-hero'));
