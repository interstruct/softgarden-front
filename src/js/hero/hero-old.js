import ScrollMagic from 'scrollmagic';
import { TimelineLite } from 'gsap/TweenMax';
import { throttle, debounce } from 'throttle-debounce';
import 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js';

import { g } from './../_global';

export default class Hero {
  constructor(hero) {
    this.hero = hero;
    this.bg = this.hero.find('.js-hero-bg');
    this.trinity = this.hero.find('.js-hero-trinity');
    this.svg = this.hero.find('.sg-trinity');
    this.lines = this.trinity.find('.js-lines path');
    this.circles = this.trinity.find('.js-circles circle');
    this.icons = this.trinity.find('.js-icons path');

    this.part = this.hero.find('.js-hero-part');
    this.text = this.hero.find('.js-hero-text');
    this.links = this.hero.find('.js-hero-link');
    this.linkLines = this.hero.find('.js-hero-link-line');

    this.heroOuterAnchor = this.hero.find('.js-anchor-outer');
    // this.heroInnerAnchor = this.hero.find('.js-anchor-inner');

    this.isAnimating = false;
    this.isReverse = false;

    this.windowHeight = $(window).height();
    this.tlStart = new TimelineLite({paused: true});
    this.tlEnd = new TimelineLite({paused: true});

    this.triggerOffset = 0.38;
    this.played = false;
    // this.calcScroll = false;
    this.animated = false;
    // this.trinityBreakPoint = this.hero.innerHeight()*this.triggerOffset;
    this.trinityBreakPoint = this.hero.innerHeight()*0.7;



    this._init();
  }


  _init() {
    this._count();
    this._scrollBehaviour();
    
    setTimeout(() => {
      // this._start();

      this._createOuterScene();
      this._createInnerScene();

      this._scrollOuterController();
      this._scrollInnerController();
      this._addHeroClicks();
    }, 10);
  }
  _scrollBehaviour() {
    let t = this.hero[0].getBoundingClientRect();
    console.log({t});

    // if( t.bottom <= 0 ) return;

    // if(t.y === 0 ) {
    //   $(window).on('scroll', this._changeTrinityOnScroll.bind(this));
    // }


    if(t.bottom >= 0) {
      console.log('must scroll');
      // g.heroInnerController.scrollTo(0);
      // g.heroInnerController.scrollTo(0);
      // window.onbeforeunload = function() { window.scrollTo(0,0); };
      $('html, body').animate({scrollTop: 0}, 1);
      // if(g.isDesktop) {
      //   $(window).on('scroll', this._changeTrinityOnScroll.bind(this));
      // }

      setTimeout(() => {
        this._start();
      },10);

    }else{
      this._hiddenPlay();
    }
  }
  // _changeTrinityOnScroll(e) {
  //   console.log(e);
  //   if(this.animated === true ) return;
  //   let offset = this.trinity.offset().top + this.trinity.innerHeight()/2;
  //   if( offset <= this.trinityBreakPoint) {

  //   }else{
  //     this.animated = true;
  //     this.trinity.css({
  //       'position': 'absolute',
  //       'top': `${this.trinityBreakPoint + 1250}px`
  //     });
  //   }
  // }
  _count() {
    this.bgHeight = this.bg.outerHeight();
    // this.bgHeight = this.hero.outerHeight()/2;
    this.padding = $('header').outerHeight();
  }
  _hiddenPlay() {
    this.trinity.css({
      'position': 'absolute',
      // 'top': `${this.trinityBreakPoint}px`
      'top': `${this.trinityBreakPoint*0.9}px`
    });
    this.played = true;
    this.tlStart
      .to(this.svg, 0, {opacity: 1}, 0)
      .to(this.part, 0, {opacity: 1}, 0)
      .to(this.text, 0, {opacity: 1}, 0)
      .to(this.svg, 0, {rotation: 0}, 0)
      .to(this.circles, 0, {opacity: 1}, 0)
      .to(this.lines, 0, {opacity: 1}, 0)
      .to(this.icons, 0, {opacity: 1}, 0)
      .to(this.links, 0, {opacity: 1}, 0)
      .to(this.linkLines, 0, {opacity: 1}, 0);
    $('.js-hero-headline').addClass('is-vertical-centered');
    this.tlStart.play();
    console.log('hidden play');
    // this._start();
    // this._endPlay();
  }
  _start() {
    // console.log('start anim');
    this.tlStart
      .fromTo(this.svg, 0.75, {opacity: 0}, {opacity: 1, ease: Power2.easeInOut})
      .fromTo(this.svg, 0.75, {rotation: 15}, {rotation: 130, ease: Power2.easeInOut})
      .staggerTo(this.lines, 0.25, {opacity: 1, ease: Power2.easeInOut}, 0.125, '-=1.5')
      .staggerTo(this.part, 0.75, {opacity: 1, ease: Power2.easeInOut}, 0.375)
      .fromTo(this.text, 0.375, {opacity: 0}, {opacity: 1, ease: Power2.easeInOut}, '-=.375');

    this.tlStart.play();
  }

  _endPlay() {
    // console.log('end play');
    this.tlEnd = null;
    this.tlEnd = new TimelineLite({paused: true});
    this.tlEnd
      .to($('.js-hero-headline'), 0.35, {opacity: 0, ease: Power2.easeInOut}, 0)
      .fromTo(this.heroInnerAnchor, 0.35, {opacity: 1}, {opacity: 0, ease: Power2.easeInOut}, 0)
      .fromTo(this.svg, 0.75, {rotation: 130}, {rotation: 0, ease: Power2.easeInOut},'-=.35')
      .fromTo(this.lines, 0.75, {opacity: 1}, {opacity: 0.5, ease: Power2.easeInOut}, '-=.75')
      .staggerTo(this.circles, 0.25, {opacity: 1, ease: Power2.easeInOut}, 0.125)
      .staggerTo(this.icons, 0.25, {opacity: 1, ease: Power2.easeInOut}, 0.125, '-=.375')
      .staggerTo(this.links, 0.5, {opacity: 1, y: 0, ease: Power2.easeInOut}, 0.25, '-=.625')
      .staggerTo(this.linkLines, 0.5, {y: 0, ease: Power2.easeInOut, onComplete: setTextPos}, 0.25, '-=1.65');

    // this.tlStart.pause(0.75);
    this.tlEnd.play();
    // this.tlStart.play();
    function setTextPos() {
      $('.js-hero-headline').addClass('is-vertical-centered');
    }
  }

  // _endReverse() {

  //   this.progress = this.tlEnd.progress();
  //   if (this.progress < 1) {
  //     this.tlEndForward.pause();
  //     this.tlEnd.reverse(this.progress*this.tlEnd.totalTime());
  //   }
  //   else {
  //     this.tlEnd.reverse(1);
  //   }
  // }

  _createOuterScene() {
    this.heroOuterScene = new ScrollMagic.Scene({
      triggerElement: this.hero[0],
      triggerHook: 0,
      duration: this.windowHeight
    })
      // .setPin(this.bg[0])
      .on('enter', e => {

        if(g.videoBg !== undefined) {
          $('.js-hero-bg-inner').find('img').addClass('is-visible');
          g.videoBg.get(0).play();
          setTimeout(() => {
            $('.js-hero-bg-inner').addClass('change-view');
          }, 1500);
        }else{
          $('.js-hero-bg-inner').find('img').addClass('is-visible');
        }
        console.log('after play in hero.js');
      })
      .addIndicators({
        name: 'hero outer scene',
        colorStart: '#dffdda',
        colorEnd: '#dffdda'
      })
      .addTo(g.heroInnerController);
  }

  _createInnerScene() {
    this.heroInnerScene = new ScrollMagic.Scene({
      triggerElement: this.hero[0],
      triggerHook: 0,
      // duration: this.bgHeight/2,
      duration: this.bgHeight*this.triggerOffset,
      offset: this.padding
    })
      // .on('enter', e => {
      //   console.log('enter');
      //   if (e.scrollDirection !== 'FORWARD') {
      //     this._endReverse();
      //   }
      // })
      .on('leave', e => {
        if(this.played === true) return;

        if (e.scrollDirection === 'FORWARD') {

          this._makeTrinityStatic();
          // setTimeout(() => {
          this._endPlay();
          this.played = true;
          // }, 100);
        }
      })
      .addIndicators({
        name: 'hero inner scene',
        colorStart: '#d33dda',
        colorEnd: '#d33dda',
        indent: 200
      })
      .addTo(g.heroInnerController);
  }
  _makeTrinityStatic() {
    let trinityOffset = this.trinity.offset().top + this.trinity.innerHeight()/2;
    // let trinityOffset = $(window).scrollTop() + this.trinity.innerHeight()/2;
    // console.log(this.hero.innerHeight());
    // console.log(this.trinityBreakPoint);
    // console.log(trinityOffset);
    trinityOffset = trinityOffset > this.trinityBreakPoint ? this.trinityBreakPoint : trinityOffset;
    // trinityOffset = trinityOffset > this.hero.innerHeight() ? this.trinityBreakPoint : trinityOffset;
    // console.log(trinityOffset);
    this.trinity.css({
      'position': 'absolute',
      // 'top': `${this.trinityBreakPoint}px`
      'top': `${trinityOffset}px`
    });
  }
  _scrollInnerController() {
    g.heroInnerController.scrollTo((newpos) => {
      let scrollTop = newpos;

      $('html, body').animate({scrollTop: newpos}, 800);
    });
  }

  _scrollOuterController() {
    g.heroInnerController.scrollTo((newpos) => {
      let scrollTop = newpos - $('.js-product-nav').outerHeight() + this.padding + 1;

      $('html, body').animate({scrollTop: scrollTop}, 800);
    });
  }

  _scrollAnchorLink() {
    
  }
  _navigateHero(e) {
    e.preventDefault();
    console.log('navvvv');
    e.preventDefault();
    g.heroInnerController.scrollTo(this.bg.innerHeight()*0.5);
  }

  _scrollToProducts(e) {
    e.preventDefault();
    const href = $(e.currentTarget).attr('href');
    g.heroInnerController.scrollTo(href);
  }

  _scrollToSections(e) {
    const href = $(e.currentTarget).attr('href');
    // this._scrollTo(href);
    g.controller.scrollTo(href);
  }

  _addHeroClicks() {
    this.heroOuterAnchor.on('click', this._scrollToProducts.bind(this));
    // this.heroInnerAnchor.on('click', this._navigateHero.bind(this));
    // this.links.on('click', this._scrollToSections.bind(this));
    this.links.on('click', e => {
      e.preventDefault();
      this._scrollToSections(e);
    });
  }
  _scrollTo(newpos) {
    console.log(newpos);
    // g.controller.scrollTo((newpos) => {
    let offset = $(`${newpos}`).offset().top;
    console.log(offset);
    // let scrollTop = offset - ($('header').outerHeight()+$('.js-product-nav').outerHeight()) + 1;
    let scrollTop = offset - ($('header').outerHeight()) + 1;

    $('html, body').animate({scrollTop: scrollTop}, 800, () => {
      g.isDropdownClicked = false;
      g.scrollToValue = scrollTop;
    });
    // });
  }

}
