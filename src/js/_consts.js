const breakpoints = {
  'desktop': 1024,
  'tablet': 960,
  'mobile': 768,
};

module.exports = {
  breakpoints: breakpoints
};
